package tamaslosonczi.rssreader.util;

import android.databinding.BindingAdapter;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import tamaslosonczi.rssreader.R;

/**
 * Created by tamas_losonczi on 16/09/17.
 */

public final class BindingUtils {

    private BindingUtils() {

    }

    @BindingAdapter("simpleListLayout")
    public static void setSimpleListLayout(RecyclerView recyclerView, boolean setup) {
        if (setup) {
            recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL));
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(recyclerView.getContext());
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            recyclerView.setLayoutManager(linearLayoutManager);
            recyclerView.setHasFixedSize(true);
        }
    }

    @BindingAdapter("hideOnEmptyText")
    public static void hideOnEmptyText(View view, String text) {
        if (TextUtils.isEmpty(text)) {
            view.setVisibility(View.GONE);
        } else {
            view.setVisibility(View.VISIBLE);
        }
    }

    @BindingAdapter("htmlText")
    public static void setHtmlText(TextView view, String text) {
        CharSequence value;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            value = Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY);
        } else {
            value = Html.fromHtml(text);
        }
        view.setText(value);
    }

    @BindingAdapter("loadImage")
    public static void loadImage(ImageView imageView, String url) {
        GlideApp.with(imageView.getContext())
                .load(url)
                .placeholder(R.drawable.ic_rss_feed_black_24dp)
                .into(imageView);
    }
}
