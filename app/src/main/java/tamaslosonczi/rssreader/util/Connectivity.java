package tamaslosonczi.rssreader.util;

import android.content.Context;
import android.net.ConnectivityManager;

/**
 * Created by tamas_losonczi on 20/09/17.
 */

public class Connectivity {

    private final Context context;

    public Connectivity(Context context) {
        this.context = context;
    }

    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnectedOrConnecting();
    }

}
