package tamaslosonczi.rssreader.util;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

/**
 * Created by tamas_losonczi on 18/09/17.
 */

@GlideModule
public class ImageCacheModule extends AppGlideModule {}
