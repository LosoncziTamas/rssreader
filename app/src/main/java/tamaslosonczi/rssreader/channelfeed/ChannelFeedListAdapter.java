package tamaslosonczi.rssreader.channelfeed;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import tamaslosonczi.rssreader.ChannelEntry;
import tamaslosonczi.rssreader.R;
import tamaslosonczi.rssreader.databinding.ChannelFeedListItemBinding;

/**
 * Created by tamas_losonczi on 17/09/17.
 */

public class ChannelFeedListAdapter extends RecyclerView.Adapter<ChannelFeedListAdapter.ViewHolder>  {

    private final List<ChannelEntry> items;
    private final ChannelFeedView view;

    public ChannelFeedListAdapter(ChannelFeedView view, List<ChannelEntry> items) {
        this.view = view;
        this.items = items;
    }

    @Override
    public ChannelFeedListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ChannelFeedListItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.channel_feed_list_item,
                parent,
                false);

        return new ChannelFeedListAdapter.ViewHolder(binding);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public void onBindViewHolder(ChannelFeedListAdapter.ViewHolder holder, int position) {
        holder.binding.setData(items.get(position));
        holder.binding.setView(view);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final ChannelFeedListItemBinding binding;

        public ViewHolder(ChannelFeedListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

}
