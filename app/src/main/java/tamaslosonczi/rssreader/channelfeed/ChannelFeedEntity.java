package tamaslosonczi.rssreader.channelfeed;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.ObservableArrayList;

import java.util.List;

import cz.kinst.jakub.view.SimpleStatefulLayout;
import tamaslosonczi.rssreader.BR;
import tamaslosonczi.rssreader.ChannelEntry;

/**
 * Created by tamas_losonczi on 19/09/17.
 */

public class ChannelFeedEntity extends BaseObservable {

    private String state;

    private final List<ChannelEntry> entries;

    public ChannelFeedEntity() {
        state = SimpleStatefulLayout.State.CONTENT;
        entries = new ObservableArrayList<>();
    }

    @Bindable
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
        notifyPropertyChanged(BR.state);
    }

    @Bindable
    public List<ChannelEntry> getEntries() {
        return entries;
    }
}
