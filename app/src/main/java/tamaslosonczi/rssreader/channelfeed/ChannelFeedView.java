package tamaslosonczi.rssreader.channelfeed;

import com.tamaslosonczi.viewmodel.IView;

import tamaslosonczi.rssreader.ChannelEntry;

/**
 * Created by tamas_losonczi on 17/09/17.
 */

public interface ChannelFeedView extends IView {
    void onFeedEntryClick(ChannelEntry entry);
}
