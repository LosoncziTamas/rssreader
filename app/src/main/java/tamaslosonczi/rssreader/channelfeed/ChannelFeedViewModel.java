package tamaslosonczi.rssreader.channelfeed;

import com.rometools.rome.feed.synd.SyndEntry;
import com.rometools.rome.feed.synd.SyndFeed;
import com.rometools.rome.io.SyndFeedInput;
import com.rometools.rome.io.XmlReader;
import com.tamaslosonczi.viewmodel.AbstractViewModel;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import cz.kinst.jakub.view.SimpleStatefulLayout;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import tamaslosonczi.rssreader.ChannelEntry;
import tamaslosonczi.rssreader.RssChannel;
import tamaslosonczi.rssreader.util.Connectivity;

/**
 * Created by tamas_losonczi on 17/09/17.
 */

public class ChannelFeedViewModel extends AbstractViewModel<ChannelFeedView> {

    public final ChannelFeedEntity entity;

    private final SyndFeedInput syndFeedInput;
    private final Connectivity connectivity;

    ChannelFeedViewModel(ChannelFeedEntity entity, Connectivity connectivity) {
        this.entity = entity;
        this.connectivity = connectivity;
        syndFeedInput = new SyndFeedInput();
    }

    void loadFeed(final RssChannel channel) {
        if (!connectivity.isOnline()) {
            entity.setState(SimpleStatefulLayout.State.OFFLINE);
            return;
        }

        entity.setState(SimpleStatefulLayout.State.PROGRESS);

        Single<List<ChannelEntry>> single = Single.fromCallable(() -> {
            SyndFeed syndFeed = syndFeedInput.build(new XmlReader(new URL(channel.getUrl())));
            return getEntriesFromFeed(syndFeed);
        });

        single.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSingleObserver<List<ChannelEntry>>() {
                    @Override
                    public void onSuccess(@NonNull List<ChannelEntry> channelEntries) {
                        entity.getEntries().clear();
                        entity.getEntries().addAll(channelEntries);
                        showContent();
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        entity.setState(SimpleStatefulLayout.State.OFFLINE);
                    }
                });
    }

    private List<ChannelEntry> getEntriesFromFeed(SyndFeed syndFeed) {
        List<ChannelEntry> entries = new ArrayList<>();
        for (SyndEntry syndEntry : syndFeed.getEntries()) {
            entries.add(new ChannelEntry(syndEntry));
        }
        return entries;
    }

    private void showContent() {
        if (entity.getEntries().isEmpty()) {
            entity.setState(SimpleStatefulLayout.State.EMPTY);
        } else {
            entity.setState(SimpleStatefulLayout.State.CONTENT);
        }
    }
}
