package tamaslosonczi.rssreader.channelfeed;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuItem;

import com.tamaslosonczi.viewmodel.base.ViewModelBaseActivity;

import org.parceler.Parcels;

import tamaslosonczi.rssreader.ChannelEntry;
import tamaslosonczi.rssreader.R;
import tamaslosonczi.rssreader.RssChannel;
import tamaslosonczi.rssreader.content.ContentActivity;
import tamaslosonczi.rssreader.databinding.ActivityChannelFeedBinding;
import tamaslosonczi.rssreader.util.Connectivity;

import static tamaslosonczi.rssreader.util.ViewUtils.setupActionBar;

/**
 * Created by tamas_losonczi on 17/09/17.
 */

public class ChannelFeedActivity extends ViewModelBaseActivity<ChannelFeedView, ChannelFeedViewModel> implements ChannelFeedView {

    private static final String ARGS_CHANNEL = "args_channel";

    public static Intent newIntent(Context context, RssChannel rssChannel) {
        Intent intent = new Intent(context, ChannelFeedActivity.class);
        intent.putExtra(ARGS_CHANNEL, Parcels.wrap(rssChannel));
        return intent;
    }

    private ActivityChannelFeedBinding binding;
    private RssChannel rssChannel;

    @NonNull
    @Override
    public ChannelFeedViewModel createViewModel() {
        return new ChannelFeedViewModel(new ChannelFeedEntity(), new Connectivity(this));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initUi();
        getViewModel().loadFeed(getArg());
    }

    private void initUi() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_channel_feed);
        binding.setView(this);
        binding.setViewModel(getViewModel());
        setModelView(this);
        setupAdapter();
        setupActionBar(getSupportActionBar());
        binding.container.setOfflineRetryOnClickListener(view -> getViewModel().loadFeed(getArg()));
        setTitle(getArg().getTitle());
    }

    private void setupAdapter() {
        ChannelFeedListAdapter adapter = new ChannelFeedListAdapter(this, getViewModel().entity.getEntries());
        binding.listChannelFeed.setAdapter(adapter);
    }

    @Override
    public void onFeedEntryClick(ChannelEntry entry) {
        startActivity(ContentActivity.newIntent(this, entry));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_channel_feed, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_refresh) {
            getViewModel().loadFeed(getArg());
            return true;
        } else if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private RssChannel getArg() {
        if (rssChannel == null) {
            rssChannel = Parcels.unwrap(getIntent().getParcelableExtra(ARGS_CHANNEL));
        }
        return rssChannel;
    }

}
