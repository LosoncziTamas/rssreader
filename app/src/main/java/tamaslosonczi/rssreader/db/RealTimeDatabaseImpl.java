package tamaslosonczi.rssreader.db;

import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;

import java.util.Collections;
import java.util.List;

import tamaslosonczi.rssreader.RssChannel;

/**
 * Created by tamas_losonczi on 19/09/17.
 */

public class RealTimeDatabaseImpl implements RealTimeDatabase {

    private static final String TAG = RealTimeDatabaseImpl.class.getCanonicalName();
    private static final String RSS_CHANNELS_PATH = "rss_channels";

    private final DatabaseReference databaseReference;

    public RealTimeDatabaseImpl() {
        databaseReference = FirebaseDatabase.getInstance().getReference();
    }

    @Override
    public void load(final Callback callback) {
        databaseReference.child(RSS_CHANNELS_PATH)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        GenericTypeIndicator<List<RssChannel>> typeIndicator = new GenericTypeIndicator<List<RssChannel>>() {};
                        List<RssChannel> cachedChannels = dataSnapshot.getValue(typeIndicator);
                        if (cachedChannels == null) {
                            cachedChannels = Collections.emptyList();
                        }
                        callback.onCacheLoaded(cachedChannels);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        callback.onCacheLoaded(Collections.emptyList());
                        Log.e(TAG, "Cache load failed.");
                    }
                });
    }

    @Override
    public void update(final List<RssChannel> channels) {
        databaseReference.child(RSS_CHANNELS_PATH).setValue(channels);
    }
}
