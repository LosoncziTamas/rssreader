package tamaslosonczi.rssreader.db;

import java.util.List;

import tamaslosonczi.rssreader.RssChannel;

/**
 * Created by tamas_losonczi on 19/09/17.
 */

public interface RealTimeDatabase {

    interface Callback {
        void onCacheLoaded(List<RssChannel> cachedChannels);
    }

    void load(final Callback callback);

    void update(final List<RssChannel> channels);

}
