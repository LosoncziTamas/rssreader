package tamaslosonczi.rssreader.main;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.ObservableArrayList;

import java.util.List;

import cz.kinst.jakub.view.SimpleStatefulLayout;
import tamaslosonczi.rssreader.BR;
import tamaslosonczi.rssreader.RssChannel;

/**
 * Created by tamas_losonczi on 18/09/17.
 */

public class MainEntity extends BaseObservable {

    private String state;
    private final List<RssChannel> channels;


    public MainEntity() {
        state = SimpleStatefulLayout.State.CONTENT;
        channels = new ObservableArrayList<>();
    }

    @Bindable
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
        notifyPropertyChanged(BR.state);
    }

    @Bindable
    public List<RssChannel> getChannels() {
        return channels;
    }
}
