package tamaslosonczi.rssreader.main;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v7.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;

import com.tamaslosonczi.viewmodel.base.ViewModelBaseActivity;

import tamaslosonczi.rssreader.R;
import tamaslosonczi.rssreader.RssChannel;
import tamaslosonczi.rssreader.channelfeed.ChannelFeedActivity;
import tamaslosonczi.rssreader.databinding.ActivityMainBinding;
import tamaslosonczi.rssreader.db.RealTimeDatabaseImpl;
import tamaslosonczi.rssreader.util.Connectivity;

import static tamaslosonczi.rssreader.util.ViewUtils.toast;

public class MainActivity extends ViewModelBaseActivity<MainView, MainViewModel> implements MainView, ActionMode.Callback {

    private ActivityMainBinding binding;
    private MainListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initUi();
        getViewModel().loadRssChannels();
    }

    private void initUi() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.setView(this);
        binding.setViewModel(getViewModel());
        setModelView(this);
        setupAdapter();
        setSupportActionBar(binding.toolbar);
        binding.container.setOfflineRetryOnClickListener(view -> getViewModel().loadRssChannels());
    }

    @Override
    public void onFabClick() {
        new AddChannelDialog(getViewModel(), this).show();
    }

    private void setupAdapter() {
        adapter = new MainListAdapter(this, getViewModel().entity.getChannels());
        binding.listChannel.setAdapter(adapter);
    }

    @Override
    public void showError(@StringRes int errorMsgRes) {
        toast(this, errorMsgRes);
    }

    @Override
    public void onChannelClick(RssChannel rssChannel) {
        startActivity(ChannelFeedActivity.newIntent(this, rssChannel));
    }

    @Override
    public void switchToDeleteMode() {
        startSupportActionMode(this);
    }

    @NonNull
    @Override
    public MainViewModel createViewModel() {
        return new MainViewModel(new RealTimeDatabaseImpl(), new MainEntity(), new Connectivity(this));
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        mode.getMenuInflater().inflate(R.menu.menu_action_mode, menu);
        getWindow().setStatusBarColor(getResources().getColor(R.color.action_mode_status_bar_color));
        adapter.setDeleting(true);
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_delete:
                getViewModel().permanentlyDeleteItems(adapter.getSelectedItems());
                mode.finish();
                return true;
            default:
                return false;
        }
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
        getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        adapter.setDeleting(false);
    }
}
