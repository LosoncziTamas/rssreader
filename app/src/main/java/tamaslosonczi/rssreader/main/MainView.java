package tamaslosonczi.rssreader.main;

import android.support.annotation.StringRes;

import com.tamaslosonczi.viewmodel.IView;

import tamaslosonczi.rssreader.RssChannel;

/**
 * Created by tamas_losonczi on 16/09/17.
 */

public interface MainView extends IView {
    void onFabClick();

    void showError(@StringRes int errorMsgRes);

    void onChannelClick(RssChannel rssChannel);

    void switchToDeleteMode();
}
