package tamaslosonczi.rssreader.main;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import tamaslosonczi.rssreader.BR;
import tamaslosonczi.rssreader.R;
import tamaslosonczi.rssreader.RssChannel;

/**
 * Created by tamas_losonczi on 16/09/17.
 */

public class MainListAdapter extends RecyclerView.Adapter<MainListAdapter.ViewHolder> {

    private final List<RssChannel> items;
    private final SparseBooleanArray selectedItems;
    private final MainView mainView;

    private boolean deleting;

    public MainListAdapter(MainView mainView, List<RssChannel> items) {
        this.mainView = mainView;
        this.items = items;
        selectedItems = new SparseBooleanArray();
    }

    public void setDeleting(boolean deleting) {
        this.deleting = deleting;
        if (!deleting) {
            selectedItems.clear();
        }
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewDataBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.main_list_item,
                parent,
                false);

        return new ViewHolder(binding);
    }

    public SparseBooleanArray getSelectedItems() {
        return selectedItems;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.binding.setVariable(BR.data, items.get(position));
        holder.binding.setVariable(BR.view, mainView);
        holder.binding.getRoot().setActivated(selectedItems.get(position));
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener {
        private final ViewDataBinding binding;

        public ViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            binding.getRoot().setOnLongClickListener(this);
        }

        @Override
        public boolean onLongClick(View view) {
            int position = getAdapterPosition();
            view.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS);
            if (deleting) {
                boolean selected = selectedItems.get(position);
                selectedItems.put(position, !selected);
                notifyDataSetChanged();
            } else {
                selectedItems.put(position, true);
                mainView.switchToDeleteMode();
            }
            return true;
        }
    }
}