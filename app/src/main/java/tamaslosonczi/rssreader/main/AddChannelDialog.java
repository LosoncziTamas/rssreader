package tamaslosonczi.rssreader.main;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.widget.EditText;
import android.widget.LinearLayout;

import tamaslosonczi.rssreader.R;

/**
 * Created by tamas_losonczi on 18/09/17.
 */

public class AddChannelDialog {

    interface Callback {
        void onChannelUrlEntered(String url);
    }

    private final Callback callback;
    private final Context context;

    public AddChannelDialog(Callback callback, Context context) {
        this.callback = callback;
        this.context = context;
    }

    public void show() {
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle(R.string.add_channel_dialog_title);
        alert.setMessage(R.string.add_channel_dialog_message);

        LinearLayout container = new LinearLayout(context);
        container.setOrientation(LinearLayout.VERTICAL);
        int padding = context.getResources().getDimensionPixelSize(R.dimen.double_space);
        container.setPadding(padding, 0, padding, 0);
        final EditText editText = new EditText(context);
        editText.setHint(R.string.add_channel_dialog_hint);
        container.addView(editText);

        alert.setView(container);
        alert.setPositiveButton(R.string.add_channel_dialog_ok, (dialog, whichButton) -> callback.onChannelUrlEntered(editText.getText().toString()));
        alert.setNegativeButton(R.string.add_channel_dialog_cancel, null);
        alert.show();
    }
}
