package tamaslosonczi.rssreader.main;

import android.util.SparseBooleanArray;
import android.webkit.URLUtil;

import com.rometools.rome.feed.synd.SyndFeed;
import com.rometools.rome.io.SyndFeedInput;
import com.rometools.rome.io.XmlReader;
import com.tamaslosonczi.viewmodel.AbstractViewModel;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import cz.kinst.jakub.view.SimpleStatefulLayout;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import tamaslosonczi.rssreader.R;
import tamaslosonczi.rssreader.RssChannel;
import tamaslosonczi.rssreader.db.RealTimeDatabase;
import tamaslosonczi.rssreader.util.Connectivity;

/**
 * Created by tamas_losonczi on 16/09/17.
 */

public class MainViewModel extends AbstractViewModel<MainView> implements AddChannelDialog.Callback {

    public final MainEntity entity;

    private final RealTimeDatabase realTimeDatabase;
    private final SyndFeedInput syndFeedInput;
    private final Connectivity connectivity;

    MainViewModel(RealTimeDatabase realTimeDatabase, MainEntity entity, Connectivity connectivity) {
        this.realTimeDatabase = realTimeDatabase;
        this.entity = entity;
        this.connectivity = connectivity;
        syndFeedInput = new SyndFeedInput();
    }

    void loadRssChannels() {
        if (!connectivity.isOnline()) {
            entity.setState(SimpleStatefulLayout.State.OFFLINE);
            return;
        }

        entity.setState(SimpleStatefulLayout.State.PROGRESS);
        realTimeDatabase.load(cachedChannels -> {
            entity.getChannels().clear();
            entity.getChannels().addAll(cachedChannels);
            showContent();
        });
    }

    private void showContent() {
        if (entity.getChannels().isEmpty()) {
            entity.setState(SimpleStatefulLayout.State.EMPTY);
        } else {
            entity.setState(SimpleStatefulLayout.State.CONTENT);
        }
    }

    private void fetchFeedContent(final String url) {
        Single<SyndFeed> single = Single.fromCallable(() -> syndFeedInput.build(new XmlReader(new URL(url))));

        single.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSingleObserver<SyndFeed>() {
                    @Override
                    public void onSuccess(@NonNull SyndFeed syndFeed) {
                        RssChannel result = new RssChannel(url, syndFeed);
                        entity.getChannels().add(result);
                        realTimeDatabase.update(entity.getChannels());
                        showContent();
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        if (connectivity.isOnline()) {
                            showContent();
                            getView().showError(R.string.invalid_rss_message);
                        } else {
                            entity.setState(SimpleStatefulLayout.State.OFFLINE);
                        }
                    }
                });
    }

    private boolean isDuplicate(String url) {
        for (RssChannel rssChannel : entity.getChannels()) {
            if (rssChannel.getUrl().trim().equalsIgnoreCase(url.trim())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onChannelUrlEntered(String url) {
        boolean valid = URLUtil.isValidUrl(url);

        if (!valid) {
            getView().showError(R.string.invalid_url_message);
        } else if (isDuplicate(url)) {
            getView().showError(R.string.duplicate_url_message);
        } else {
            entity.setState(SimpleStatefulLayout.State.PROGRESS);
            fetchFeedContent(url);
        }
    }

    void permanentlyDeleteItems(SparseBooleanArray deleteFlags) {
        List<RssChannel> itemsToRemove = new ArrayList<>();
        for (int i = 0; i < entity.getChannels().size(); ++i) {
            if (deleteFlags.get(i, false)) {
                itemsToRemove.add(entity.getChannels().get(i));
            }
        }
        entity.getChannels().removeAll(itemsToRemove);
        realTimeDatabase.update(entity.getChannels());
        showContent();
    }
}
