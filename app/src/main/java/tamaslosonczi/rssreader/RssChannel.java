package tamaslosonczi.rssreader;

import com.rometools.rome.feed.synd.SyndFeed;

import org.parceler.Parcel;

/**
 * Created by tamas_losonczi on 16/09/17.
 */

@Parcel
public class RssChannel {

    String title;
    String description;
    String imageUrl;
    String url;

    public RssChannel() {
    }

    public RssChannel(String url, SyndFeed syndFeed) {
        this.url = url;
        description = syndFeed.getDescription();
        title = syndFeed.getTitle();
        imageUrl = syndFeed.getImage() == null ? "" : syndFeed.getImage().getUrl();

    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getUrl() {
        return url;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RssChannel that = (RssChannel) o;

        if (!title.equals(that.title)) return false;
        if (description != null ? !description.equals(that.description) : that.description != null)
            return false;
        if (imageUrl != null ? !imageUrl.equals(that.imageUrl) : that.imageUrl != null)
            return false;
        return url.equals(that.url);

    }

    @Override
    public int hashCode() {
        int result = title.hashCode();
        result = 31 * result + url.hashCode();
        return result;
    }
}
