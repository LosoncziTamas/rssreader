package tamaslosonczi.rssreader;

import android.text.TextUtils;

import com.rometools.rome.feed.synd.SyndEntry;

import org.parceler.Parcel;

import java.text.SimpleDateFormat;

/**
 * Created by tamas_losonczi on 16/09/17.
 */

@Parcel
public class ChannelEntry {

    private static final SimpleDateFormat ENTRY_DATE = new SimpleDateFormat("EEE, d MMM HH:mm");

    String content;
    String title;
    String webUrl;
    String authorAndPublishDate;

    public ChannelEntry() {
    }

    public ChannelEntry(SyndEntry syndEntry) {
        this.title = syndEntry.getTitle();
        this.webUrl = syndEntry.getLink();

        if (!syndEntry.getContents().isEmpty()) {
            this.content = syndEntry.getContents().get(0).getValue();
        }
        setAuthorAndPublishDate(syndEntry);
    }

    private void setAuthorAndPublishDate(SyndEntry syndEntry) {
        boolean hasAuthor = !TextUtils.isEmpty(syndEntry.getAuthor());
        boolean hasDate = syndEntry.getPublishedDate() != null;
        if (hasAuthor && hasDate) {
            authorAndPublishDate = String.format("%s - %s", syndEntry.getAuthor(), ENTRY_DATE.format(syndEntry.getPublishedDate()));
        } else if (hasAuthor) {
            authorAndPublishDate = syndEntry.getAuthor();
        } else if (hasDate) {
            authorAndPublishDate = ENTRY_DATE.format(syndEntry.getPublishedDate());
        }
    }

    public String getContent() {
        return content;
    }

    public String getTitle() {
        return title;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public String getAuthorAndPublishDate() {
        return authorAndPublishDate;
    }
}
