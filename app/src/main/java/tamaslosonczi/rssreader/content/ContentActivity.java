package tamaslosonczi.rssreader.content;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;

import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

import org.parceler.Parcels;

import tamaslosonczi.rssreader.ChannelEntry;
import tamaslosonczi.rssreader.R;
import tamaslosonczi.rssreader.databinding.ActivityContentBinding;

import static tamaslosonczi.rssreader.util.ViewUtils.setupActionBar;

/**
 * Created by tamas_losonczi on 17/09/17.
 */

public class ContentActivity extends AppCompatActivity {

    private static final String ARGS_ENTRY = "args_entry";

    public static Intent newIntent(Context context, ChannelEntry channelEntry) {
        Intent intent = new Intent(context, ContentActivity.class);
        intent.putExtra(ARGS_ENTRY, Parcels.wrap(channelEntry));
        return intent;
    }

    private ActivityContentBinding binding;
    private ChannelEntry channelEntry;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_content);
        ChannelEntry entry = getArg();
        if (TextUtils.isEmpty(entry.getContent())) {
            binding.reader.loadWebUrl(entry.getWebUrl());
        } else {
            binding.reader.loadEntry(entry);
        }
        setupActionBar(getSupportActionBar());
        setTitle(getArg().getTitle());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_content, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_share) {
            ShareLinkContent content = new ShareLinkContent.Builder()
                    .setContentUrl(Uri.parse(getArg().getWebUrl()))
                    .build();
            ShareDialog.show(this, content);
            return true;
        } else if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private ChannelEntry getArg() {
        if (channelEntry == null) {
            channelEntry = Parcels.unwrap(getIntent().getParcelableExtra(ARGS_ENTRY));
        }
        return channelEntry;
    }
}
