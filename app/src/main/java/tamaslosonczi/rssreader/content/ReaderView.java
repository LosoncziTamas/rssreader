package tamaslosonczi.rssreader.content;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.text.Html;
import android.util.AttributeSet;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import tamaslosonczi.rssreader.ChannelEntry;
import tamaslosonczi.rssreader.R;

/**
 * Created by tamas_losonczi on 18/09/17.
 */

public class ReaderView extends WebView {

    private static final String CSS_PREFIX = "<link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\" />" + "<meta name='viewport' content='width=device-width'/>";

    private static int MESSAGE_DELAY = 1000;

    public ReaderView(Context context) {
        this(context, null);
    }

    public ReaderView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ReaderView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setWebViewClient(new WebViewClient());
        setHorizontalScrollBarEnabled(false);
        getSettings().setUseWideViewPort(true);
        getSettings().setLoadWithOverviewMode(true);
    }

    private boolean containsMediaTypes(String content) {
        final boolean[] contains = {false};
        Html.fromHtml(content, null, (b, s, editable, xmlReader) -> {
            if (s.equals("img") || s.equals("video") || s.equals("script") || s.equals("audio")) {
                contains[0] = true;
            }
        });
        return contains[0];
    }

    private void showMessage(ChannelEntry entry) {
        postDelayed(() -> {
            Snackbar message = Snackbar.make(ReaderView.this, R.string.reader_media_type_message, Snackbar.LENGTH_LONG);
            message.setAction(R.string.reader_media_type_action, view -> loadWebUrl(entry.getWebUrl()));
            message.show();
        }, MESSAGE_DELAY);
    }

    public void loadEntry(ChannelEntry entry) {
        if (containsMediaTypes(entry.getContent())) {
            showMessage(entry);
        }
        getSettings().setJavaScriptEnabled(false);
        loadDataWithBaseURL("file:///android_asset/", CSS_PREFIX + entry.getContent(), "text/html", "UTF-8", null);
    }

    public void loadWebUrl(String url) {
        getSettings().setJavaScriptEnabled(true);
        loadUrl(url);
    }
}
